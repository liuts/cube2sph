#!/usr/bin/bash

## job name and output file
#SBATCH --job-name specfem_mesher
#SBATCH --output %j.o

###########################################################
# USER PARAMETERS

## 40 CPUs ( 10*4 ), walltime 5 hour
#SBATCH --nodes=10
#SBATCH --ntasks=400
#SBATCH --time=0:15:00

###########################################################

cd $SLURM_SUBMIT_DIR

#module load gcc/7.3.0 openmpi/3.1.1

#module load intel/2018.3 intelmpi/2018.3

module load intel openmpi hdf5/1.8.21 netcdf/4.6.3

NPROC=`grep ^NPROC DATA/Par_file | grep -v -E '^[[:space:]]*#' | cut -d = -f 2`

echo "start to change model on GLL points: `date`"
# runs database generation

mpirun -np $NPROC ./bin/setup_model_cartesian

if [[ $? -ne 0 ]]; then exit 1; fi
echo "changing model done: `date`"
