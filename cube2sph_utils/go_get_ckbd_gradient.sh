#!/usr/bin/bash

## job name and output file
#SBATCH --job-name specfem_mesher
#SBATCH --output %j.o

###########################################################
# USER PARAMETERS

## 40 CPUs ( 10*4 ), walltime 5 hour
#SBATCH --nodes=10
#SBATCH --ntasks=400
#SBATCH --time=00:15:00

###########################################################

cd $SLURM_SUBMIT_DIR

#module load gcc/7.3.0 openmpi/3.1.1

#module load intel/2018.3 intelmpi/2018.3

module load intel openmpi

NPROC=`grep ^NPROC DATA/Par_file | grep -v -E '^[[:space:]]*#' | cut -d = -f 2`

mpirun -np $NPROC ./bin/create_ckbd_gradient ckbd ../../vs_ckbd
