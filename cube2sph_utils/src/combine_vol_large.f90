program combine_vol_large
  !! program to generate .vtk binary file 
  !! Tianshi Liu, 2022.2
  !! use mpi, but is a seriel code
  use mpi
  use postprocess_par, only: CUSTOM_REAL,NGLLX,NGLLY,NGLLZ,NDIM, &
                             MAX_STRING_LEN,IIN,IOUT,MAX_KERNEL_NAMES
  implicit none
  character(len=MAX_STRING_LEN) :: kernel_names(MAX_KERNEL_NAMES)
  character(len=MAX_STRING_LEN) :: kernel_names_comma_delimited
  character(len=MAX_STRING_LEN) :: prname_lp,filename,arg_str,in_dir,out_fn
  character(len=12) :: str1, str2
  character(len=1) :: endl = char(10)
  integer, parameter :: NARGS = 5
  integer, parameter :: NPTS_PER_ELEMENT = 8
  integer :: nker, nprocs, rank_min, rank_max
  integer :: rank, nspec, i, j, k, ispec, nglob, nspec_irregular, iker, ipt, iglob
  integer :: nspec_total = 0
  integer, dimension(:), allocatable :: nspec_rank 
  integer, dimension(NPTS_PER_ELEMENT) :: ix, iy, iz
  real, dimension(:), allocatable :: xstore, ystore, zstore
  real, dimension(:,:,:,:),allocatable :: x, y, z, val
  integer, dimension(:,:,:,:), allocatable :: ibool
  integer :: ier
  call MPI_Init(ier)
  call MPI_Comm_size(MPI_COMM_WORLD, nprocs, ier)
  if (nprocs > 1) then
    stop 'only allow seriel run'
  endif
  ! parse command line arguments
  if (command_argument_count() /= NARGS) then
    print *,'USAGE:  ./bin/xcombine_vol_large IN_DIR OUT_FN KERNEL_NAME RANK_MIN RANK_MAX'
    stop 'Please check command line arguments'
  endif
  call get_command_argument(1, in_dir)
  call get_command_argument(2, out_fn)
  call get_command_argument(3, kernel_names_comma_delimited)
  call parse_kernel_names(kernel_names_comma_delimited,kernel_names,nker)
  call get_command_argument(4, arg_str)
  read(arg_str, *) rank_min
  call get_command_argument(5, arg_str)
  read(arg_str, *) rank_max
  allocate(nspec_rank(rank_min:rank_max))
  !! loop through ranks to get number of elements
  do rank = rank_min, rank_max
    write(prname_lp,'(a,i6.6,a)') trim(in_dir)// '/' //'proc',rank,'_'
    filename = prname_lp(1:len_trim(prname_lp))//'external_mesh.bin'
    open(unit=IIN,file=trim(filename),status='unknown',action='read',form='unformatted',iostat=ier)
    if (ier /= 0) stop 'error opening database proc######_external_mesh.bin'
    read(IIN) nspec
    close(IIN)
    nspec_rank(rank) = nspec
  enddo
  nspec_total = sum(nspec_rank(:))
  print *, 'found ', nspec_total, ' elements in all ranks'

  !ix = (/1,5,5,1,1,5,5,1,3,5,3,1,3,5,3,1,1,5,5,1/)
  !iy = (/1,1,5,5,1,1,5,5,1,3,5,3,1,3,5,3,1,1,5,5/)
  !iz = (/1,1,1,1,5,5,5,5,1,1,1,1,5,5,5,5,3,3,3,3/)
  ix = (/1,5,5,1,1,5,5,1/)
  iy = (/1,1,5,5,1,1,5,5/)
  iz = (/1,1,1,1,5,5,5,5/)
  !! open vtk file for write
  open(unit=IOUT, file=trim(out_fn), form='binary', convert='BIG_ENDIAN')
  print *, 'writing to ', trim(out_fn)
  !! write file header
  write(IOUT) trim('# vtk DataFile Version 2.0'//endl)
  write(IOUT) trim('SPECFEM3D mesh'//endl)
  write(IOUT) trim('BINARY'//endl)
  write(IOUT) trim('DATASET UNSTRUCTURED_GRID'//endl//endl)

  !! points
  write(str1(1:12),'(i12)') nspec_total*NPTS_PER_ELEMENT
  write(IOUT) trim('POINTS '//str1//'  float'//endl)
  do rank = rank_min, rank_max
    write(prname_lp,'(a,i6.6,a)') trim(in_dir)// '/' //'proc',rank,'_'
    filename = prname_lp(1:len_trim(prname_lp))//'external_mesh.bin'
    open(unit=IIN,file=trim(filename),status='unknown',action='read',form='unformatted',iostat=ier)
    read(IIN) nspec
    allocate(ibool(NGLLX, NGLLY, NGLLZ, nspec), &
           x(NGLLX, NGLLY, NGLLZ, nspec), &
           y(NGLLX, NGLLY, NGLLZ, nspec), &
           z(NGLLX, NGLLY, NGLLZ, nspec), stat=ier)
    read(IIN) nglob
    allocate(xstore(nglob), ystore(nglob), zstore(nglob), stat=ier)
    read(IIN) nspec_irregular
    read(IIN) ibool
    read(IIN) xstore
    read(IIN) ystore
    read(IIN) zstore
    close(IIN)
    do ispec = 1,nspec
      do k = 1,NGLLZ
        do j = 1,NGLLY
          do i = 1,NGLLX
            iglob = ibool(i,j,k,ispec)
            x(i,j,k,ispec) = xstore(iglob)
            y(i,j,k,ispec) = ystore(iglob)
            z(i,j,k,ispec) = zstore(iglob)
          enddo
        enddo
      enddo
    enddo
    deallocate(xstore, ystore, zstore, ibool)
    do ispec = 1, nspec
      do ipt = 1, NPTS_PER_ELEMENT
        write(IOUT) x(ix(ipt),iy(ipt),iz(ipt),ispec), &
                    y(ix(ipt),iy(ipt),iz(ipt),ispec), &
                    z(ix(ipt),iy(ipt),iz(ipt),ispec)
      enddo
    enddo
    deallocate(x, y, z)
  enddo

  !! cells
  write(str1(1:12), '(i12)') nspec_total
  write(str2(1:12), '(i12)') nspec_total * (1 + NPTS_PER_ELEMENT)
  write(IOUT) trim(endl//endl//'CELLS '//str1//' '//str2//endl)
  do ispec = 1, nspec_total
    write(IOUT) NPTS_PER_ELEMENT, ((ispec-1)*NPTS_PER_ELEMENT+ipt, ipt=0,NPTS_PER_ELEMENT-1)
  enddo

  !! cell_types
  write(str1(1:12), '(i12)') nspec_total
  write(IOUT) trim(endl//endl//'CELL_TYPES '//str1//endl)
  write(IOUT) (12, ispec=1, nspec_total)

  !! point data
  do iker = 1, nker
    write(str1(1:12), '(i12)') nspec_total*NPTS_PER_ELEMENT
    write(IOUT) trim(endl//endl//'POINT_DATA '//str1//endl)
    write(IOUT) trim('SCALARS '//trim(kernel_names(iker))//' float'//endl)
    write(IOUT) trim('LOOKUP_TABLE default'//endl)
    do rank = rank_min, rank_max
      write(prname_lp,'(a,i6.6,a)') trim(in_dir)// '/' //'proc',rank,'_'
      filename = prname_lp(1:len_trim(prname_lp))//trim(kernel_names(iker))//'.bin'
      open(unit=IIN,file=trim(filename),status='unknown',action='read',form='unformatted',iostat=ier)
      allocate(val(NGLLX, NGLLY, NGLLZ, nspec_rank(rank)))
      read(IIN) val
      close(IIN)
      do ispec = 1, nspec_rank(rank)
        do ipt = 1, NPTS_PER_ELEMENT
          write(IOUT) val(ix(ipt),iy(ipt),iz(ipt),ispec)
        enddo
      enddo
      deallocate(val)
    enddo
  enddo
  deallocate(nspec_rank)
  close(IOUT)
  call MPI_Finalize(ier)
end program combine_vol_large
