#### this script outputs the boundary of the cubed sphere
#### as a .gmt file 
import numpy as np
from xyz2latlond import *
n_sample = 100
M_PER_DEGREE = 111194.925
samples = np.array(range(0, n_sample), dtype=np.double) / n_sample
# MODIFY mesh size, mesh center and rotation
CENTER_LAT = 62.5
CENTER_LON = -151.0
ROTATION_AZI = 20.0
xmin = - M_PER_DEGREE * 11.0
xmax = M_PER_DEGREE * 11.0
ymin = - M_PER_DEGREE * 11.0
ymax = M_PER_DEGREE * 11.0
#############################################
R = 6371000.0
#R = 637100.0
DEGREE_PER_RAD = 180.0 / np.pi
alpha = CENTER_LON / DEGREE_PER_RAD
beta = np.pi / 2.0 -CENTER_LAT / DEGREE_PER_RAD
gamma = ROTATION_AZI / DEGREE_PER_RAD
cosa = np.cos(alpha)
sina = np.sin(alpha)
cosb = np.cos(beta)
sinb = np.sin(beta)
cosg = np.cos(gamma)
sing = np.sin(gamma)
M11 = cosg * cosb * cosa - sing * sina
M12 = - sing* cosb * cosa - cosg * sina
M13 = sinb * cosa
M21 = cosg * cosb * sina + sing * cosa
M22 = - sing * cosb * sina + cosg * cosa
M23 = sinb * sina
M31 = - cosg * sinb
M32 = sing * sinb
M33 = cosb
f_bound = open('tomo_bound.gmt', 'w')
## xmin
f_bound.write('# xmin\n')
f_bound.write('>\n')
x = xmin
y = samples * (ymax - ymin) + ymin
z = 0.0
xi = x / R
eta = y / R
t1 = np.tan(xi)
t2 = np.tan(eta)
z = (1.0 + z / R) / np.sqrt(1.0 + t1 * t1 + t2 * t2)
x = - z * t2 * R
y = z * t1 * R
z = z * R
xnew = M11 * x + M12 * y + M13 * z
ynew = M21 * x + M22 * y + M23 * z
znew = M31 * x + M32 * y + M33 * z
for i in range(0, n_sample):
    lat, lon, d = xyz2latlond(xnew[i], ynew[i], znew[i])
    f_bound.write('%20.10f %20.10f\n' % (lon - 360.0, lat))
## xmax
f_bound.write('# xmax\n')
f_bound.write('>\n')
x = xmax
y = samples * (ymax - ymin) + ymin
z = 0.0
xi = x / R
eta = y / R
t1 = np.tan(xi)
t2 = np.tan(eta)
z = (1.0 + z / R) / np.sqrt(1.0 + t1 * t1 + t2 * t2)
x = - z * t2 * R
y = z * t1 * R
z = z * R
xnew = M11 * x + M12 * y + M13 * z
ynew = M21 * x + M22 * y + M23 * z
znew = M31 * x + M32 * y + M33 * z
for i in range(0, n_sample):
    lat, lon, d = xyz2latlond(xnew[i], ynew[i], znew[i])
    f_bound.write('%20.10f %20.10f\n' % (lon - 360.0, lat))
## ymin
f_bound.write('# ymin\n')
f_bound.write('>\n')
x = samples * (xmax - xmin) + xmin
y = ymin
z = 0.0
xi = x / R
eta = y / R
t1 = np.tan(xi)
t2 = np.tan(eta)
z = (1.0 + z / R) / np.sqrt(1.0 + t1 * t1 + t2 * t2)
x = - z * t2 * R
y = z * t1 * R
z = z * R
xnew = M11 * x + M12 * y + M13 * z
ynew = M21 * x + M22 * y + M23 * z
znew = M31 * x + M32 * y + M33 * z
for i in range(0, n_sample):
    lat, lon, d = xyz2latlond(xnew[i], ynew[i], znew[i])
    f_bound.write('%20.10f %20.10f\n' % (lon - 360.0, lat))
## ymax
f_bound.write('# ymax\n')
f_bound.write('>\n')
x = samples * (xmax - xmin) + xmin
y = ymax
z = 0.0
xi = x / R
eta = y / R
t1 = np.tan(xi)
t2 = np.tan(eta)
z = (1.0 + z / R) / np.sqrt(1.0 + t1 * t1 + t2 * t2)
x = - z * t2 * R
y = z * t1 * R
z = z * R
xnew = M11 * x + M12 * y + M13 * z
ynew = M21 * x + M22 * y + M23 * z
znew = M31 * x + M32 * y + M33 * z
for i in range(0, n_sample):
    lat, lon, d = xyz2latlond(xnew[i], ynew[i], znew[i])
    f_bound.write('%20.10f %20.10f\n' % (lon - 360.0, lat))
f_bound.close()
